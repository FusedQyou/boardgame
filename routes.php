<?php

// localhost
if ($_SERVER['HTTP_HOST'] == 'localhost:8888') {

    // GET routes
    $router->get('', 'controllers/index.php');
    $router->get('home', 'controllers/index.php');
    $router->get('players', 'controllers/players.php');
    $router->get('games', 'controllers/games.php');
    $router->get('users', 'controllers/users.php');
    $router->get('battles', 'controllers/battles.php');

    // POST routes
    $router->post('add_player', 'controllers/add_player.php');
}

// server
else {
    $urlRoute = '~s1115933/P1_OOAPP_Opdracht';

    // GET routes
    $router->get($urlRoute, 'controllers/index.php');
    $router->get($urlRoute. '/home', 'controllers/index.php');
    $router->get($urlRoute. '/players', 'controllers/players.php');
    $router->get($urlRoute. '/games', 'controllers/games.php');
    $router->get($urlRoute. '/users', 'controllers/users.php');
    $router->get($urlRoute. '/battles', 'controllers/battles.php');

    // POST routes
    $router->post($urlRoute. '/add_player', 'controllers/add_player.php');
}