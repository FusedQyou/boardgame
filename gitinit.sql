SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `2018_P1_07_gezinshuis`
--

-- Create database

-- >> USE FOR LOCAL DATABASE
-- CREATE DATABASE IF NOT EXISTS `boardgame` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
-- USE `boardgame`;

-- >> USE FOR SERVER DATABASE
-- CREATE DATABASE IF NOT EXISTS `S1115933_boardgame` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
-- USE `S1115933_boardgame`;




-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `profile`
--

CREATE TABLE IF NOT EXISTS `profile` (
  `id` varchar(36) NOT NULL,
  `firstName` varchar(30) DEFAULT NULL,
  `middleName` varchar(30) DEFAULT NULL,
  `lastName` varchar(30) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `role` tinyint(4) DEFAULT 0,
  `dob` date DEFAULT NULL,
  `proficiency` varchar(1024) NOT NULL,
  `reason` varchar(1024) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `profile`
  ADD PRIMARY KEY (`id`);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` varchar(36) NOT NULL,
  `email` varchar(128) DEFAULT NULL,
  `password` varchar(256) DEFAULT NULL,
  `creation_date` date DEFAULT NULL,
  `status` tinyint(4) DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Gegevens worden geëxporteerd voor tabel `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `creation_date`, `status`) VALUES
('admin', 'admin@boardgame.com', '8C6976E5B5410415BDE908BD4DEE15DFB167A9C873FC4BB8A81F6F2AB448A918', '0000-00-00', 1);