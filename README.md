# Boardgame app, by Roy van der Wijk.

### Boardgame app for Object Oriënted Programming.
### Windesheim Flevoland Software Development 2018-2019

This is a website for a boardgame app.

It contains information about the game, the users, and highscores of the game.

While this is a website for a boardgame **there is no functioning game**.

######*Because that takes a long time. And I'm lazy. Deal with it.*

&nbsp;

### Variables
| Variable | Description |
| ------:| -----------:|
| $application | General application class. Initialised upon website loading. Contains all general information. |

&nbsp;

### Functions
| Function | Description |
| ------:| -----------:|
| composer dump-autoload | This reloads the class autoloader by composer, should you have added a new class. |
| php -S localhost:8888 | Initialise your website locally. |