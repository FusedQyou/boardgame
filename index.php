<?php

// Display PHP errormode
error_reporting(E_ALL);
ini_set('display_errors', 1);

// Load composer autoloader
require "vendor/autoload.php";

// Create a general Application class, and load the database configuration and


$query = require 'core/bootstrap.php';

// Get the current page uri
$current = Request::uri();

/**
 * Where are you in your page and where do you go with the
 * routes and controllers
 *
 * @routes.php routes to different endpoints
 * @Request::uri get the uri
 * @Request::method POST or GET?
 */
require Router::load('routes.php')->direct(Request::uri(), Request::method());