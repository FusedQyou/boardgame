<?php

$application = [];

// Get the database configuration
$application['config'] = require 'config.php';

// Create a database connection and setup the querybuilder masterclass
$application['database'] = new QueryBuilder (
    Database::CreateConnection($application['config']['database'])
);