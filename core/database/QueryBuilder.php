<?php
/**
 * Created by: stephanhoeksema 2018
 * phpoop
 */

class QueryBuilder
{
    protected $pdo;

    /**
     * Create a database connection using PDO.
     *
     * @param mixed[] $pdo set the database connection to a variable within the class.
     */

    public function __construct(PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    /**
     * Select everything from a table
     *
     * @param  string $table     the name of the table in the database.
     * @param  string $intoclass define class for output.
     * @return array             received table.
     */

    public function selectAll($table, $intoClass)
    {
        /**
         * @var $statement all data for given table
         * @var $intoClass
         */
        $statement = $this->pdo->prepare("select * from {$table}");
        $statement->execute();

        return $statement->fetchAll(PDO::FETCH_CLASS, $intoClass);

    }

}