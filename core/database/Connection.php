<?php
/**
 * @author Stephan Hoeksema
 */

class Database
{
    /**
     * Create a database connection using PDO.
     *
     * @param  mixed[] $config array structure which contains all database connection info based on the host (local / server)
     * @return PDO class with database connection.
     * @throws PDOException if the database connection could not be made.
     */

    public static function CreateConnection($config)
    {
        try {
            return new PDO(
                "",
                $config['username'],
                $config['password'],
                $config['options']
            );
        } catch (PDOException $e) {
            die("database connection could not be made<br>". $e->getMessage(). "<br><br>pls fix me :(");
        }
    }
}