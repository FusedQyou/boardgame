<?php
/**
 * Created by: stephanhoeksema 2018
 * phpoop
 */
/**
 * @database - configuration for PDO()
 */

// localhost
if ($_SERVER['HTTP_HOST'] == 'localhost:8888') {
    return [
        'database' => [
            'name' => 'boardgame',
            'username' => 'root',
            'password' => '',
            'connection' => 'mysql:host=localhost',
            'options' => [
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
            ]
        ]
    ];
}

// server
else {
    return [
        'database' => [
            'name' => 'S1115933_boardgame',
            'username' => 'S1115933',
            'password' => 'Azjs!7FgTmMtWz',
            'connection' => 'mysql:host=adsd.clow.nl',
            'options' => [
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
            ]
        ]
    ];
}
